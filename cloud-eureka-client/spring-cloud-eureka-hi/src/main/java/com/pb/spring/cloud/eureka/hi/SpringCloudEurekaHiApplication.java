package com.pb.spring.cloud.eureka.hi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class SpringCloudEurekaHiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudEurekaHiApplication.class, args);
    }

    @GetMapping("/hi")
    public String hi(String name) {
        System.out.println("Hi " + name);
        return "Hi " + name;
    }
}
