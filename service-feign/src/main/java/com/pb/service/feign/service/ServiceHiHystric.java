package com.pb.service.feign.service;

import org.springframework.stereotype.Component;

/**
 * @author PB
 */
@Component
public class ServiceHiHystric implements ServiceHi {

    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry " + name;
    }
}
